import React from 'react';

const EmojiItemRender = (props) => {
  return (
    <div>
      <div>name</div>
      <div>description</div>
    </div>
  );
};

export default (props: any) => {
  return (
    <div className="emoji-collection">
      <div className="emoji-collection-title">title: {props.title}</div>
      {props.emojis.map((item) => {
        return <EmojiItemRender emoji={item}></EmojiItemRender>;
      })}
    </div>
  );
};
