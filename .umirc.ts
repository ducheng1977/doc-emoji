import { defineConfig } from 'dumi';

const base = process.env.NODE_ENV === 'production' ? '/doc-emoji/' : '/';

export default defineConfig({
  title: 'emoji book',
  mode: 'site',
  // more config: https://d.umijs.org/config
  base: base,
  publicPath: base,
  locales: [['zh-CN', '中文']],
  navs: [
    null, // null 值代表保留约定式生成的导航，只做增量配置
    {
      title: 'GitHub',
      path: 'https://github.com/umijs/dumi',
    },
  ],
  hash: true,
  // favicon: "",
  metas: [
    {
      name: 'keywords',
      content: '吹口琴的喵~ | emoji',
    },
    {
      name: 'description',
      content: '分类整理emoji',
    },
  ],
});
