// 爬取网站的数据https://emojixd.com/，然后存储为json格式，编写组件进行渲染
import axios from 'axios';
import cheerio from 'cheerio';
import { FsUtil } from './fs-utils.mjs';

const folder_html_prefix = './spider/data-html';
const filename_html_list = './spider/data-html-list.html';
const filename_html_collection = './spider/data-html-collection.html';
const filename_json_group = './spider/data-group.json';
const filename_json_subGroup = './spider/data-subGroup.json';
const filename_json_emoji = './spider/data-emoji.json';
const filename_json_collection = './spider/data-collection.json';

const headers = {
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
  'referer': 'https://emojixd.com/group/smileys-emotion',
  'cookie': 'Hm_lvt_b1a6b0df4704d54cbd8a47499b2ae036=1642990013; Hm_lpvt_b1a6b0df4704d54cbd8a47499b2ae036=1642992086'
};

const handleAllGroups = async ($) => {
  // 获取所有的大类
  const groups = [...$('h2')].map(item => $(item).text());
  // 存储
  await FsUtil.write(filename_json_group, JSON.stringify(groups));
}

const handleAllSubGroups = async ($) => {
  const result = [];
  const groups = [...$('h2')];
  groups.forEach((item) => {
    const group = { name: $(item).text(), children: [] }; // 当前的大类group
    const subGroups = [...$(item).nextUntil('hr')]; // 获取之后的同级别元素，直到hr
    subGroups.forEach(item2 => {
      group.children.push({ name: $(item2).find('h3').text() });
    });
    result.push(group);
  })
  // 保存
  await FsUtil.write(filename_json_subGroup, JSON.stringify(result));
}

const handleAllEmojis = async ($) => {
  const result = [];
  const groups = [...$('h2')];
  groups.forEach(item => {
    const groupName = $(item).text(); // 当前的大类group
    const subGroups = [...$(item).nextUntil('hr')]; // 获取之后的同级别元素，直到hr
    subGroups.forEach(item2 => {
      const subGroupName = $(item2).find('h3').text();
      const emojis = [...$(item2).children('div')]
      emojis.forEach(item3 => {
        const image = $(item3).find('.emoji').text();
        const nameZh = $($(item3).find('.truncate')[0]).text();
        const nameEn = $($(item3).find('.truncate')[1]).text();
        const href = $(item3).find('a').attr('href');
        const emoji = { groupName, subGroupName, image, nameZh, nameEn, href, detail: {}, collections: [] };
        result.push(emoji);
      })
    });
  })
  // 保存
  await FsUtil.write(filename_json_emoji, JSON.stringify(result));
}

const handleAllCollections = async () => {
  const result = [];
  let $;
  if (FsUtil.exist(filename_html_collection)) {
    const res = await FsUtil.read(filename_html_collection, 'utf-8');
    $ = cheerio.load(res);
  } else {
    const res = await axios({
      url: 'https://emojixd.com/pockets',
      method: 'get',
      headers
    });
    await FsUtil.write(filename_html_collection, res.data);
    $ = cheerio.load(res.data);
  };
  const collectionDoms = [...$('#emoji-list').children()];
  // 循环生成children为空数组的结果集
  for (let i = 0; i < collectionDoms.length; i++) {
    const $collectionDom = $(collectionDoms[i]);
    const name = $collectionDom.find('.pocket-title').text();
    const href = $collectionDom.find('a').attr('href');
    const url = `https://emojixd.com${href}`;
    const collectionItem = { name, url, children: [] };
    result.push(collectionItem);
  }
  // 然后根据url爬取集合数据
  for (let i = 0; i < result.length; i++) {
    let $collection;
    const { url, name, children } = result[i];
    const filepath = `${folder_html_prefix}/${name}.html`;
    if (FsUtil.exist(filepath)) {
      const res = await FsUtil.read(filepath, 'utf-8');
      $collection = cheerio.load(res);
    } else {
      await FsUtil.sleep(2000); // 多等一下
      const res = await axios({ url, method: 'get', headers });
      await FsUtil.write(filepath, res.data);
      $collection = cheerio.load(res.data);
    }
    const emojiDoms = [...$collection('.emoji-item')];
    emojiDoms.forEach(item => {
      const image = $(item).find('.emoji').text();
      const nameZh = $($(item).find('.truncate')[0]).text();
      const nameEn = $($(item).find('.truncate')[1]).text();
      const href = $(item).find('a').attr('href');
      const emoji = { image, nameZh, nameEn, href };
      children.push(emoji);
    });
    console.log(`${name}插入成功`)
  }
  // 保存
  await FsUtil.write(filename_json_collection, JSON.stringify(result));
}

const handleGroupAndEmoji = async () => {
  let $;
  if (FsUtil.exist(filename_html_list)) {
    const res = await FsUtil.read(filename_html_list, 'utf-8');
    $ = cheerio.load(res);
  } else {
    const res = await axios({
      url: 'https://emojixd.com/list',
      method: 'get',
      headers
    });
    await FsUtil.write(filename_html_list, res.data);
    $ = cheerio.load(res.data);
  }
  // 处理大类
  await handleAllGroups($);
  // 处理小类
  await handleAllSubGroups($);
  // 处理emoji
  await handleAllEmojis($);
}

const handleEmojiCollections = async () => {
  const { default: collections } = await import('./data-collection.json');
  const { default: emojis } = await import('./data-emoji.json');
  for (let i = 0; i < collections.length; i++) {
    const { name: collectionName, children } = collections[i];
    children.forEach(target => {
      const emoji = emojis.find((item) => item.image === target.image);
      if (!emoji) return;
      if (!emoji.collections.includes(collectionName)) {
        emoji.collections.push(collectionName);
      }
    });
    // 保存
    await FsUtil.write(filename_json_emoji, JSON.stringify(emojis));
  }
}

const handleEmojiDetail = async () => {
  try {
    const { default: emojis } = await import('./data-emoji.json');
    for (let i = 0; i < emojis.length; i++) {
      const emoji = emojis[i];
      if (emoji.detail.code) { // 如果有详情的话，跳过
        console.log(`${i},${emoji.nameZh}已经有详细信息了`)
        continue
      };
      await FsUtil.sleep(3000);
      const url = `https://emojixd.com${emoji.href}`;
      const res = await axios({ url, method: 'get', headers });
      const $ = cheerio.load(res.data);
      const unicode_version = $($('dl dd')[2]).text();
      const unicode_code = $($('dl dd')[3]).text();
      const code = $($('dl dd')[5]).text();
      const keywords = [...$($('dl dd')[6]).find('a')].map(item => $(item).text());
      emoji.detail = { unicode_version, unicode_code, code, keywords };
      // 保存
      await FsUtil.write(filename_json_emoji, JSON.stringify(emojis));
      console.log(`${i},${emoji.nameZh}保存成功`)
    }
  } catch (err) {
    console.log(err);
    handleEmojiDetail();
  }
}

(async () => {
  // // 收集所有的group、subGroup、emoji信息
  // await handleGroupAndEmoji();
  // // 收集所有的collection信息
  // await handleAllCollections();
  // // 根据上面的结果处理emoji的collections字段
  // await handleEmojiCollections();
  // 爬取emoji的详细信息
  await handleEmojiDetail();
})();