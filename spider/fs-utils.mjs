import fs from 'fs'

export class FsUtil {
  static read(filename, options) {
    return new Promise((resolve, reject) => {
      fs.readFile(filename, options, function (err, bytesRead) {
        if (err) return reject(err)
        return resolve(bytesRead)
      });
    })
  }

  static write(filename, data, options) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filename, data, options, function (err) {
        if (err) return reject(err)
        return resolve('读取成功')
      });
    })
  }

  static exist(filename) {
    return fs.existsSync(filename)
  }

  static sleep(time) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve()
      }, time)
    })
  }
}