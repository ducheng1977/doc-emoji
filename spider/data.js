const getCollections = () => import('./data-collection.json');

export default {
  getCollections,
};
